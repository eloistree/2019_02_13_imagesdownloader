﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EloiExperiments.Toolbox.ImagesDownloader
{
    public static class ImagesDownloader
    {
        public static ImagesDownloaderInterface[] downloaderAvailable = new ImagesDownloaderInterface[] {
            new PinterestDownloader(),
            new PornHubDownloader(),
            new GoogleAlbumDownload()
    }; 

        public static IEnumerator DownloadImages(string url, out List<ImageWebReference> imagesfound) {


            yield break;
        }
        public static IEnumerator DownloadImage(ImageWebReference image, out Texture2D downloadedImage) {


            yield break;
        }
    }

    public class ImageWebReference {
        public ImageWebReference(string sourceUrl, string imageUrl) {
            m_sourceUrl = sourceUrl;
            m_imageUrl = imageUrl;
        }
        public string m_sourceUrl = "";
        public string m_imageUrl = "";
        public ImageType GetImageType() {

            throw new NotImplementedException();
        }
    }
    public enum ImageType
    {
        PNG,JPEG,Data, Other

    }
    public interface ImagesDownloaderInterface
    {

        bool IsUrlTakeInCharge(string url);
        IEnumerator DownloadImages(string url, out List<ImageWebReference> imagesfound);

    }


    public class PinterestDownloader : ImagesDownloaderInterface
    {
   
        IEnumerator ImagesDownloaderInterface.DownloadImages(string url, out List<ImageWebReference> imagesfound)
        {
            throw new NotImplementedException();
        }

        bool ImagesDownloaderInterface.IsUrlTakeInCharge(string url)
        {
            throw new NotImplementedException();
        }
    }
    public class PornHubDownloader : ImagesDownloaderInterface
    {
   

        IEnumerator ImagesDownloaderInterface.DownloadImages(string url, out List<ImageWebReference> imagesfound)
        {
            throw new NotImplementedException();
        }

        bool ImagesDownloaderInterface.IsUrlTakeInCharge(string url)
        {
            throw new NotImplementedException();
        }
    }
    public class GoogleAlbumDownload : ImagesDownloaderInterface
    {
    

        IEnumerator ImagesDownloaderInterface.DownloadImages(string url, out List<ImageWebReference> imagesfound)
        {
            throw new NotImplementedException();
        }

        bool ImagesDownloaderInterface.IsUrlTakeInCharge(string url)
        {
            throw new NotImplementedException();
        }
    }

    public class ClassicDownload : ImagesDownloaderInterface
    {
    
        IEnumerator ImagesDownloaderInterface.DownloadImages(string url, out List<ImageWebReference> imagesfound)
        {
            throw new NotImplementedException();
        }

        bool ImagesDownloaderInterface.IsUrlTakeInCharge(string url)
        {
            throw new NotImplementedException();
        }
    }
}

