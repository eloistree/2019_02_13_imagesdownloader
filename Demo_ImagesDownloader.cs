﻿using EloiExperiments.Toolbox.ImagesDownloader;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_ImagesDownloader : MonoBehaviour
{
    public RawImage m_image;
    public AspectRatioFitter m_ratio;
    public Text m_debugDisplay;

    public string [] m_urls;
    public List<ImageWebReference> m_imagesfound;
    public ImageWebReference m_current;
    public Texture2D m_texture;
    IEnumerator Start()
    {
        for (int i = 0; i < m_urls.Length; i++)
        {
            yield return ImagesDownloader.DownloadImages(m_urls[i], out m_imagesfound);
            for (int j = 0; j < m_imagesfound.Count; j++)
            {
                m_current = m_imagesfound[j];
                SetWithImageRefernce(m_current);
                yield return ImagesDownloader.DownloadImage(m_current, out m_texture);
                SetWithImage(m_texture);
                yield return new WaitForSeconds(1);
            }


        }
    }
    void SetWithImageRefernce(ImageWebReference image)
    {
        if (image == null)
            return;
        m_debugDisplay.text = m_debugDisplay.text + "\n\nS: " + image.m_sourceUrl + "\nI: " + image.m_imageUrl;
    }

    void SetWithImage(Texture2D image)
    {
        if (image == null)
            return;
        m_image.texture = m_image.texture;
        m_ratio.aspectRatio = (float)image.height / (float)image.height;


    }
}
